# Kotlin Boilerplate

A boilerplate project created in kotlin using databinding , viewmodel , observer ,LiveData with MVVM structure.

## Getting Started

The Boilerplate contains the minimal implementation required to create a new library or project. The repository code is preloaded with some basic components like basic app architecture, app theme, constants and required dependencies to create a new project. By using boiler plate code as standard initializer, we can have same patterns in all the projects that will inherit it. This will also help in reducing setup & development time by allowing you to use same code pattern and avoid re-writing from scratch.

## Folder Structure

Here is the core folder structure which kotlin provides.

```
android-app/
|- .gradel
|- .idea
|- app
|- build
|- gradel
|- build.gradel
|- gradel.properties
|- gradelw
|- gradelw.bat
|- local.properties
|- settings.gradel
|- External Libraries
|- Scratches and consoles

```

Here is the folder structure we have been using in this project

```

package name/

```


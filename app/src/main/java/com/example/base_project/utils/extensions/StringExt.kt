package com.example.base_project.utils.extensions

import android.text.Editable
import android.text.SpannableString
import android.text.style.UnderlineSpan
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

fun String?.isNotNullOrEmpty(): Boolean {
    return this != null && this.isNotEmpty()
}

fun Editable?.isNotNullOrEmpty(): Boolean {
    return this != null && this.toString() != null && this.toString().isNotEmpty()
}

fun CharSequence?.isNotNullOrEmpty(): Boolean {
    return this != null && this.toString() != null && this.toString().isNotEmpty()
}

fun String.toUnderLine(): SpannableString {
    val content = SpannableString(this)
    content.setSpan(UnderlineSpan(), 0, this.length, 0)
    return content
}

fun String.formatting(number: Int): String {
    return this.format(number)
}

fun String.formatting(str: String): String {
    return this.format(str)
}

fun String.changeYYYYMMDDToDDMMYYY(): String {
    if (this.isNullOrEmpty()) {
        return ""
    }
    val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
    val outputFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy")
    val date: Date = inputFormat.parse(this)
    val outputDateStr: String = outputFormat.format(date)
    return outputDateStr.format(date)
}

fun String.changeDDMMYYYToYYYYMMDD(): String {
    if (this.isNullOrEmpty()) {
        return ""
    }
    val inputFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy")
    val outputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
    val date: Date = inputFormat.parse(this)
    val outputDateStr: String = outputFormat.format(date)
    return outputDateStr.format(date)
}

fun String?.changeServerTimeFormatToDDMMYYY(): String {
    if (this.isNullOrEmpty()) {
        return ""
    }
    return try {
        val inputFormat: DateFormat =
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX", Locale.getDefault())
        val outputFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        val date: Date = inputFormat.parse(this)!!
        val outputDateStr: String = outputFormat.format(date)
        outputDateStr.format(date)
    } catch (e: Exception) {
        e.printStackTrace()
        ""
    }

}

fun String.getYYYYMMDDDate(): Date {
    val outputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
    return outputFormat.parse(this);
}


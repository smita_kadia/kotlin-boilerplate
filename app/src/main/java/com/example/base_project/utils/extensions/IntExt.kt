package com.example.base_project.utils.extensions

fun Int.twoDigitString(): String {
    return String.format("%02d", this)
}
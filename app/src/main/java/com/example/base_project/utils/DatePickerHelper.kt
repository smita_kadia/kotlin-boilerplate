package com.example.base_project.utils
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import com.example.base_project.utils.extensions.dateFormatDDMMYYYY
import com.example.base_project.utils.extensions.getDateDDMMYYYY
import com.example.base_project.utils.extensions.toCalendar

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import java.util.*


class DatePickerHelper(
    private val fragmentManager: FragmentManager,
    private val title: String,
    private val textView: TextView
) {
    fun show() {

        var now: Calendar = Calendar.getInstance()
        if (!textView.text.isNullOrEmpty()) {
            now = textView.text.toString().toCalendar(dateFormatDDMMYYYY)
        }

        var dpd = DatePickerDialog.newInstance(
            { view, year, monthOfYear, dayOfMonth ->
                now.set(year, monthOfYear, dayOfMonth)
                textView.text = now.getDateDDMMYYYY()
            },
            now.get(Calendar.YEAR),
            now.get(Calendar.MONTH),
            now.get(Calendar.DAY_OF_MONTH)
        )

        dpd.setTitle(title)

        dpd.setOnCancelListener { dialog ->
            Utils.log("DatePickerDialog", "Dialog was cancelled")
            dpd = null
        }

        dpd.show(fragmentManager, "Datepickerdialog" + textView.id)
    }
}
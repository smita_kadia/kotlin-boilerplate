package com.example.base_project.utils.extensions

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

const val dateFormatDDMMYYYY = "dd/MM/yyyy"

fun Calendar.getDateDDMMYYYY(): String {
    val format1 = SimpleDateFormat(dateFormatDDMMYYYY)
    return format1.format(this.time)
}

fun String.toCalendar(pattern: String): Calendar {
    val dateFormat = SimpleDateFormat(pattern, Locale.getDefault())
    return try {
        val calendar = Calendar.getInstance()
        calendar.time = dateFormat.parse(this)
        calendar
    } catch (e: ParseException) {
        e.printStackTrace()
        Calendar.getInstance()
    }
}

fun Calendar.formatTime(pattern: String): String {
    val dateFormat = SimpleDateFormat(pattern, Locale.getDefault())
    return dateFormat.format(time)
}

fun String.adjustTimePattern(oldPattern: String, newPattern: String): String? {
    val dateFormat = SimpleDateFormat(oldPattern, Locale.getDefault())
    return try {
        val calendar = Calendar.getInstance()
        calendar.time = dateFormat.parse(this)
        dateFormat.applyPattern(newPattern)
        dateFormat.format(calendar.time)
    } catch (e: ParseException) {
        throw(e)
    }
}

fun Calendar.midnight(): Calendar {
    return this.run {
        set(Calendar.HOUR_OF_DAY, 0)
        set(Calendar.MINUTE, 0)
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 0)
        return@run this
    }
}

fun Calendar.yesterday(): Calendar {
    return this.run {
        set(Calendar.DAY_OF_MONTH, -1)
        return@run this
    }
}

fun Calendar.formatToServerDateTimeDefaults(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    return sdf.format(this.time)
}

fun Calendar.formatToTruncatedDateTime(): String {
    val sdf = SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
    return sdf.format(this.time)
}

/**
 * Pattern: yyyy-MM-dd
 */
fun Calendar.formatToServerDateDefaults(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    return sdf.format(this.time)
}

/**
 * Pattern: HH:mm:ss
 */
fun Calendar.formatToServerTimeDefaults(): String {
    val sdf = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
    return sdf.format(this.time)
}

/**
 * Pattern: dd/MM/yyyy HH:mm:ss
 */
fun Calendar.formatToViewDateTimeDefaults(): String {
    val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
    return sdf.format(this.time)
}

/**
 * Pattern: dd/MM/yyyy
 */
fun Calendar.formatToViewDateDefaults(): String {
    val sdf = SimpleDateFormat(dateFormatDDMMYYYY, Locale.getDefault())
    return sdf.format(this.time)
}

/**
 * Pattern: HH:mm:ss
 */
fun Calendar.formatToViewTimeDefaults(): String {
    val sdf = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
    return sdf.format(this.time)
}

/**
 * Add field date to current date
 */
fun Calendar.add(field: Int, amount: Int): Date {
    Calendar.getInstance().apply {
        add(field, amount)
        return time
    }
}

fun Calendar.addYears(years: Int): Calendar {
    this.add(Calendar.YEAR, years)
    return this
}

fun Calendar.addMonths(months: Int): Calendar {
    this.add(Calendar.MONTH, months)
    return this
}

fun Calendar.addDays(days: Int): Calendar {
    this.add(Calendar.DAY_OF_MONTH, days)
    return this
}

fun Calendar.addHours(hours: Int): Calendar {
    this.add(Calendar.HOUR_OF_DAY, hours)
    return this
}

fun Calendar.addMinutes(minutes: Int): Calendar {
    this.add(Calendar.MINUTE, minutes)
    return this
}

fun Calendar.addSeconds(seconds: Int): Calendar {
    this.add(Calendar.SECOND, seconds)
    return this
}
package com.example.base_project.utils.helper

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import com.example.base_project.R
import com.example.base_project.databinding.LayoutDialogSimpleErrorBinding
import com.google.android.material.snackbar.Snackbar

class AlertUtil {

    companion object {



        @JvmStatic
        @BindingAdapter("snackbarError")
        fun snackbarError(view: View, error: String?) {

            if (error.isNullOrEmpty()) return

            if (error.length > 50) {

                Snackbar.make(view, error, 8 * 1000).show()

            } else {

                Snackbar.make(view, error, Snackbar.LENGTH_LONG).show()
            }
        }

        @JvmStatic
        fun showAlert(context: Context, message: String, title: String?) {

            val builder = AlertDialog.Builder(context)

            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogSimpleErrorBinding: LayoutDialogSimpleErrorBinding =
                DataBindingUtil.inflate(inflater, R.layout.layout_dialog_simple_error, null, false)

            builder.setView(dialogSimpleErrorBinding.root)

            val alertDialog = builder.create()

            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.window?.requestFeature(Window.FEATURE_NO_TITLE)

            alertDialog.setCancelable(false)
            alertDialog.show()

            dialogSimpleErrorBinding.message = message
            dialogSimpleErrorBinding.title = title

            dialogSimpleErrorBinding.viewDevider.visibility = View.GONE
            dialogSimpleErrorBinding.txtNotnow.visibility = View.GONE
            dialogSimpleErrorBinding.btnOk.setOnClickListener {
                alertDialog.dismiss()

            }
        }

        @JvmStatic
        fun showAlert(context: Context, message: String, title: String?, okClick: OkClick) {

            val builder = AlertDialog.Builder(context)

            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogSimpleErrorBinding: LayoutDialogSimpleErrorBinding =
                DataBindingUtil.inflate(inflater, R.layout.layout_dialog_simple_error, null, false)

            builder.setView(dialogSimpleErrorBinding.root)

            val alertDialog = builder.create()

            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.window?.requestFeature(Window.FEATURE_NO_TITLE)

            alertDialog.setCancelable(false)
            alertDialog.show()

            dialogSimpleErrorBinding.message = message
            dialogSimpleErrorBinding.title = title
            dialogSimpleErrorBinding.viewDevider.visibility = View.GONE
            dialogSimpleErrorBinding.txtNotnow.visibility = View.GONE
            dialogSimpleErrorBinding.btnOk.setOnClickListener {
                alertDialog.dismiss()

                okClick.onClicked()
            }
        }

        @JvmStatic
        fun showAlertDelete(context: Context, message: String, title: String?, okClick: OkClick) {

            val builder = AlertDialog.Builder(context)

            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogSimpleErrorBinding: LayoutDialogSimpleErrorBinding =
                DataBindingUtil.inflate(inflater, R.layout.layout_dialog_simple_error, null, false)

            builder.setView(dialogSimpleErrorBinding.root)

            val alertDialog = builder.create()

            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.window?.requestFeature(Window.FEATURE_NO_TITLE)

            alertDialog.setCancelable(false)
            alertDialog.show()

            dialogSimpleErrorBinding.message = message
            dialogSimpleErrorBinding.title = title
            dialogSimpleErrorBinding.viewDevider.visibility = View.VISIBLE
            dialogSimpleErrorBinding.txtNotnow.visibility = View.VISIBLE
            dialogSimpleErrorBinding.btnOk.setText(context.getString(R.string.str_yes))
            dialogSimpleErrorBinding.txtNotnow.setText(context.getString(R.string.str_no))
            dialogSimpleErrorBinding.btnOk.setOnClickListener {
                alertDialog.dismiss()

                okClick.onClicked()
            }
            dialogSimpleErrorBinding.txtNotnow.setOnClickListener {
                alertDialog.dismiss()


            }
        }

        @JvmStatic
        fun showAlertEditBack(context: Context, message: String, title: String?, okClick: OkClick) {

            val builder = AlertDialog.Builder(context)

            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogSimpleErrorBinding: LayoutDialogSimpleErrorBinding =
                DataBindingUtil.inflate(inflater, R.layout.layout_dialog_simple_error, null, false)

            builder.setView(dialogSimpleErrorBinding.root)

            val alertDialog = builder.create()

            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.window?.requestFeature(Window.FEATURE_NO_TITLE)

            alertDialog.setCancelable(false)
            alertDialog.show()

            dialogSimpleErrorBinding.message = message
            dialogSimpleErrorBinding.title = title
            dialogSimpleErrorBinding.viewDevider.visibility = View.VISIBLE
            dialogSimpleErrorBinding.txtNotnow.visibility = View.VISIBLE
            dialogSimpleErrorBinding.btnOk.setText(context.getString(R.string.str_yes))
            dialogSimpleErrorBinding.txtNotnow.setText(context.getString(R.string.str_no))
            dialogSimpleErrorBinding.btnOk.setOnClickListener {
                alertDialog.dismiss()

                okClick.onClicked()
            }
            dialogSimpleErrorBinding.txtNotnow.setOnClickListener {
                alertDialog.dismiss()


            }
        }


        fun showAlertLogout(context: Context, message: String, okClick: OkClick) {

            val builder = AlertDialog.Builder(context)

            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            /*   val dialogSimpleErrorBinding: DialogLogoutOkCancelBinding =
                   DataBindingUtil.inflate(inflater, R.layout.dialog_logout_ok_cancel, null, false)
   */
            val dialogSimpleErrorBinding: LayoutDialogSimpleErrorBinding =
                DataBindingUtil.inflate(inflater, R.layout.layout_dialog_simple_error, null, false)

            builder.setView(dialogSimpleErrorBinding.root)

            val alertDialog = builder.create()

            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.window?.requestFeature(Window.FEATURE_NO_TITLE)

            alertDialog.setCancelable(false)
            alertDialog.show()

            dialogSimpleErrorBinding.message = message
            dialogSimpleErrorBinding.btnOk.setText(context.getString(R.string.str_yes))
            dialogSimpleErrorBinding.txtNotnow.setText(context.getString(R.string.str_no))

            dialogSimpleErrorBinding.btnOk.setOnClickListener {
                alertDialog.dismiss()
                okClick.onClicked()

            }

            dialogSimpleErrorBinding.txtNotnow.setOnClickListener(View.OnClickListener {
                alertDialog.dismiss()
            })
        }



    }

    interface OkClick {

        fun onClicked()
    }

    interface ItemValue {
        fun onGetValue(value: String)
    }


    interface ItemSelected {

        fun onSelected(position: Int)
    }

    interface ChooserDismiss {

        fun onDismiss(shouldFinish: Boolean)
    }

}
package com.example.base_project.utils.extensions

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

fun <T> T.serializeToMap(): MutableMap<String, Any> {
    return convert()
}

//convert a map to a data class
inline fun <reified T> MutableMap<String, Any>.toDataClass(): T {
    return convert()
}

//convert an object of type I to type O
inline fun <I, reified O> I.convert(): O {
    val json = Gson().toJson(this)
    return Gson().fromJson(json, object : TypeToken<O>() {}.type)
}
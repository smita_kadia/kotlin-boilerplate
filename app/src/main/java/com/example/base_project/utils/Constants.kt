package com.samplekotlinprojectsetup.utils

object Constants {

    const val AUTH_TOKEN: String = "auth_token"
    const val USER_ID: String = "user_id"
    const val USER_ROLE: String = "role"
    const val HEADER_KEY: String = "header"

    const val PREF_PRIVILEGE: String = "Privilages"

    val INTENT_FORCED_LOGOUT: String? = "ForceLogout"
    val INTENT_ORDER_DATA: String? = "ForceLogout"

    val NO: String? = "no"
    val YES: String? = "yes"
    val VERIFIED: String? = "verified"
    val NOT_VERIFIED: String? = "not verified"

    const val IS_LOGGED_IN: String = "is_logged_in"

    const val DB_NAME: String = "SampleAppDatabase"

    val WEB_FAIL: String? = "fail"
    val WEB_FAIL_ERROR: String? = "webCallError"



}
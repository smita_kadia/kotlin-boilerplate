package com.example.base_project.utils.extensions

import android.widget.TextView

fun TextView.toUnderLine() {
    this.text = this.text.toString().toUnderLine()
}

fun TextView.toAddLineSeparator() {
    this.text = this.text.toString().replace("\\\n", System.getProperty("line.separator"));
}
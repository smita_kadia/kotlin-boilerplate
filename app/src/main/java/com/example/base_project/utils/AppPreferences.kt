package com.example.base_project.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences


class AppPreferences constructor(
    context: Context,
    preferencesName: String
) {

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(preferencesName, Context.MODE_PRIVATE)
    private var editor: SharedPreferences.Editor = sharedPreferences.edit()
    private val languagePreferences: SharedPreferences? =
        context.getSharedPreferences(Common.SHARED_PREF_LANG, Context.MODE_PRIVATE)

    fun getString(name: String): String? {
        return sharedPreferences.getString(name, "")
    }

    fun getString(name: String, defValue: String): String? {
        return sharedPreferences.getString(name, defValue)
    }

    @SuppressLint("CommitPrefEdits")
    fun putString(name: String, value: String) {
        editor = sharedPreferences.edit()
        editor.putString(name, value)
        editor.apply()
    }

    fun getBoolean(name: String): Boolean {
        return sharedPreferences.getBoolean(name, false)
    }

    fun getBoolean(name: String, defValue: Boolean): Boolean {
        return sharedPreferences.getBoolean(name, defValue)
    }

    @SuppressLint("CommitPrefEdits")
    fun putBoolean(name: String, value: Boolean) {
        editor = sharedPreferences.edit()
        editor.putBoolean(name, value)
        editor.apply()
    }


    fun clearAll() {
        sharedPreferences.edit().clear().apply()
    }


    fun setLangEnglish(b: Boolean) {
        if (languagePreferences != null) {
            editor = languagePreferences.edit()
        }
        editor.putBoolean("isEnglish", b)
        editor.apply()
    }

    fun isLangEnglish(): Boolean? {
        return languagePreferences?.getBoolean("isEnglish", true)
    }

    fun hasLang(): Boolean? {
        return languagePreferences?.getBoolean("hasLang", false)
    }

    fun clearLangs() {
        languagePreferences?.edit()
            ?.clear()
            ?.apply()
    }


    fun hasLang(b: Boolean) {
        if (languagePreferences != null) {
            editor = languagePreferences.edit()
        }
        editor.putBoolean("hasLang", b)
        editor.apply()
    }

    private val savedUserName = "SavedUserName"

    fun saveUserName(b: String) {
        if (languagePreferences != null) {
            editor = languagePreferences.edit()
        }
        editor.putString(savedUserName, b)
        editor.apply()
    }

    fun getSavedUsername(): String? {
        return languagePreferences?.getString(savedUserName, "")
    }

    private val savedPassword = "SavedPassword"

    fun savePassword(b: String) {
        if (languagePreferences != null) {
            editor = languagePreferences.edit()
        }
        editor.putString(savedPassword, b)
        editor.apply()
    }

    fun getSavedPassword(): String? {
        return languagePreferences?.getString(savedPassword, "")
    }

}
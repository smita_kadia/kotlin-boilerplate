package com.example.base_project.utils


import android.app.Dialog
import android.content.Context
import android.view.Window
import com.example.base_project.R
import java.util.*

class ProgressAlertUtils {
    internal var callback: AlertUtilsListener? = null

    interface AlertUtilsListener

    companion object {
        var dialog: Dialog? = null

        private val max = 28
        private val min = 0
        fun showCustomProgressDialog(con: Context) {
            // create a Dialog component

            //  DebugLog.e("this is called from home ");

            try {
                val random = Random()
                random.nextInt(max - min)
                dialog = Dialog(con)
                dialog!!.setCancelable(false)
                // this line removes title
                dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                //tell the Dialog to use the dialog.xml as it's layout description
                dialog!!.setContentView(R.layout.layout_custom_loader)


                dialog!!.show()
            } catch (e: Exception) {
                e.printStackTrace()
                dismissDialog()
            }

        }// end of showCustomProgressDialog

        fun dismissDialog() {
            try {
                if (dialog != null && dialog!!.isShowing) {
                    dialog!!.dismiss()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

    }
}

package com.example.base_project.utils.helper

import androidx.lifecycle.LiveData
import com.example.base_project.base.internet.InternetStatus
import com.example.base_project.base.internet.InternetStatusLiveData

class LiveDataUtil {

    companion object {


        /**Safe unboxing [LiveData<Boolean>]. If value of the [LiveData] is null then returns the default
         * value of type Boolean.
         *
         * @param liveData [LiveData] to be unboxed.
         * @return [LiveData]'s value if not <code>null</code> else return <code>false</code>*/
        fun safeUnbox(liveData: LiveData<Boolean>): Boolean {

            return liveData.value ?: return false
        }

        /**unboxing common [LiveDataInt><. If value of the [LiveData] is null then returns the default]
         * value of type Int.
         *
         * @param liveData [LiveData] to be unboxed.
         * @return [LiveData]'s value if not <code>null</code> else return <code>0</code>*/
        fun safeUnbox(liveData: LiveData<Int>): Int {

            return liveData.value ?: return 0
        }

        /**Safe unboxing [LiveData<Double>]. If value of the [LiveData] is null then returns the default
         * value of type Double.
         *
         * @param liveData [LiveData] to be unboxed.
         * @return [LiveData]'s value if not <code>null</code> else return <code>0.00</code>*/
        fun safeUnbox(liveData: LiveData<Double>): Double {

            return liveData.value ?: return 0.00
        }

        /**Safe unboxing [LiveData<Float>]. If value of the [LiveData] is null then returns the default
         * value of type Float.
         *
         * @param liveData [LiveData] to be unboxed.
         * @return [LiveData]'s value if not <code>null</code> else return <code>0.00f</code>*/
        fun safeUnbox(liveData: LiveData<Float>): Float {

            return liveData.value ?: return 0.00f
        }

        /**Safe unboxing [LiveData<String>]. If value of the [LiveData] is null then returns the default
         * value of type String.
         *
         * @param liveData [LiveData] to be unboxed.
         * @return [LiveData]'s value if not <code>null</code> else return <code>""</code>*/
        fun safeUnbox(liveData: LiveData<String>): String {

            return liveData.value ?: return ""
        }

        /**Safe unboxing [LiveData<Short>]. If value of the [LiveData] is null then returns the default
         * value of type Short.
         *
         * @param liveData [LiveData] to be unboxed.
         * @return [LiveData]'s value if not <code>null</code> else return <code>0</code>*/
        fun safeUnbox(liveData: LiveData<Short>): Short {

            return liveData.value ?: return 0
        }

        /**Safe unboxing [LiveData<Byte>]. If value of the [LiveData] is null then returns the default
         * value of type Byte.
         *
         * @param liveData [LiveData] to be unboxed.
         * @return [LiveData]'s value if not <code>null</code> else return <code>0</code>*/
        fun safeUnbox(liveData: LiveData<Byte>): Byte {

            return liveData.value ?: return 0
        }

        /**Safe unboxing [LiveData<Char>]. If value of the [LiveData] is null then returns the default
         * value of type Char.
         *
         * @param liveData [LiveData] to be unboxed.
         * @return [LiveData]'s value if not <code>null</code> else return <code>"".first()</code>*/
        fun safeUnbox(liveData: LiveData<Char>): Char {

            return liveData.value ?: return "".first()
        }

        fun safeUnbox(liveData: InternetStatusLiveData): InternetStatus {
            return liveData.value ?: return InternetStatus(false, InternetStatus.Type.NONE)
        }
    }
}
package com.example.base_project.utils.extensions

import android.graphics.Shader
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.View


fun View.setRepeatDashedLine(drawable: Drawable) {
    val back: BitmapDrawable = drawable as BitmapDrawable
    back.tileModeY = Shader.TileMode.REPEAT
    this.background = back
}

/** Set the View visibility to VISIBLE and eventually animate the View alpha till 100% */
fun View.visible(animate: Boolean = false) {
    if (animate) {
//        animate().alpha(1f).setDuration(300).setListener(object : AnimatorListenerAdapter() {
//            override fun onAnimationStart(animation: Animator) {
//                super.onAnimationStart(animation)
//                visibility = View.VISIBLE
//            }
//        })

//        slideAnimation(SlideDirection.UP, SlideType.SHOW)
        visibility = View.VISIBLE

    } else {
        visibility = View.VISIBLE
    }
}

/** Set the View visibility to INVISIBLE and eventually animate view alpha till 0% */
fun View.invisible(animate: Boolean = false) {
    hide(View.INVISIBLE, animate)
}

/** Set the View visibility to GONE and eventually animate view alpha till 0% */
fun View.gone(animate: Boolean = false) {
    hide(View.GONE, animate)
}

/** Convenient method that chooses between View.visible() or View.invisible() methods */
fun View.visibleOrInvisible(show: Boolean, animate: Boolean = false) {
    if (show) visible(animate) else invisible(animate)
}

/** Convenient method that chooses between View.visible() or View.gone() methods */
fun View.visibleOrGone(show: Boolean, animate: Boolean = false) {
    if (show) visible(animate) else gone(animate)
}

private fun View.hide(hidingStrategy: Int, animate: Boolean = false) {
    if (animate) {
//        animate().alpha(0f).setDuration(300).setListener(object : AnimatorListenerAdapter() {
//            override fun onAnimationEnd(animation: Animator) {
//                super.onAnimationEnd(animation)
//                visibility = hidingStrategy
//            }
//        })

//        slideAnimation(SlideDirection.DOWN, SlideType.HIDE)
        visibility = hidingStrategy
    } else {
        visibility = hidingStrategy
    }
}
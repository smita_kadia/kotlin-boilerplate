package com.example.base_project.utils

import android.app.Activity
import android.app.ActivityManager
import android.content.*
import android.content.ClipboardManager
import android.content.res.Resources
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.SystemClock
import android.provider.Settings
import android.text.*
import android.text.format.DateUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.ViewTreeObserver
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.webkit.MimeTypeMap
import android.widget.ListView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.example.base_project.BuildConfig
import com.example.base_project.base.ActBase
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.net.URLEncoder
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.regex.Matcher


class Utils(private val activity: Activity) {

    fun hideKeyboard() {
        // Check if no view has focus:
        val view1 = activity.currentFocus
        if (view1 != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view1.windowToken, 0)
        }
    }

    fun showKeyboard() {
        val view1 = activity.currentFocus
        if (view1 != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view1, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    fun requestFocus(view: View) {
        if (view.requestFocus()) {
            activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    companion object {
        val maxLogSize = 3000
        private val TAG = Utils::class.java.simpleName
        var dateInputPattern: String = "yyyy-MM-dd HH:mm:ss"
        var dateOutPutPattern: String = "dd/MM/yyyy"

        fun getFilePath(activity: Activity, pathType: Int): String? {
            val basePath = Environment.getExternalStorageDirectory().toString() + "/Android/data/" +
                    activity.packageName
            return "$basePath/pdf/discussion/"
        }

       /* fun requestMultiiplePermission(
            baseActivity: ActBase<*, *>,
            listPermissions: MutableList<String>,
            permissionAllGranted: PermissionAllGranted
        ) {
            Dexter.withActivity(baseActivity!!).withPermissions(listPermissions)
                .withListener(object :
                    MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report?.areAllPermissionsGranted()!!) {
                            permissionAllGranted.allPermissionGranted()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?,
                        token: PermissionToken?
                    ) {
                        token!!.continuePermissionRequest()
                    }

                })
                .onSameThread()
                .check()
        }*/

        fun openSettingsDialog(context: Context) {

            val builder = AlertDialog.Builder(context)
            builder.setTitle("Required Permissions")
            builder.setMessage("This app require permission to use awesome feature. Grant them in app settings.")
            builder.setPositiveButton(
                "Take Me To SETTINGS",
                DialogInterface.OnClickListener { dialog, _ ->
                    dialog.cancel()
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    val uri = Uri.fromParts("package", context.packageName, null)
                    intent.data = uri
                    context.startActivity(intent)
                })
            builder.setNegativeButton("Cancel",
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })
            builder.show()

        }

        fun extractLinks(text: String?): Array<String>? {
            val links: MutableList<String> = ArrayList()
            val m: Matcher = Patterns.WEB_URL.matcher(text)
            while (m.find()) {
                val url: String = m.group()
//                Log.d("TAGAA", "URL extracted: $url")
                links.add(url)
            }
            return links.toTypedArray()
        }

        fun redirectToBrowser(context: Context, url: String) {
            if (url.isNotEmpty()) {
                val httpIntent = Intent(Intent.ACTION_VIEW)
                httpIntent.data = Uri.parse(url)
                context.startActivity(httpIntent)
            }
        }

        fun checkStringIsNullOrEmpty(string: String): String {
            if (string == null) {
                return ""
            } else if (string.isEmpty()) {
                return ""
            }
            return string
        }

        fun openDocument(context: Context, documentUrl: String?) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(documentUrl))
            context.startActivity(browserIntent)
        }

        fun setWindowFlag(activity: Activity, bits: Int, on: Boolean) {
            val win = activity.window
            val winParams = win.attributes
            if (on) {
                winParams.flags = winParams.flags or bits
            } else {
                winParams.flags = winParams.flags and bits.inv()
            }
            win.attributes = winParams
        }

        fun isValidEmail(email: String): Boolean {
            return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        fun log(TAG: String, msg: String) {
            if (BuildConfig.DEBUG) {
                if (BuildConfig.DEBUG) {
                    for (i in 0..msg.length / maxLogSize) {
                        val start = i * maxLogSize
                        var end = (i + 1) * maxLogSize
                        end = if (end > msg.length) msg.length else end
                        Log.e(TAG, msg.substring(start, end))
                    }
                }
            }
        }

        // Sharing details via mail app (5th March 2020)
        fun onEmailIntentShare(
            activity: Activity,
            email: String,
            subject: String,
            extraText: String?
        ) {
            val emailIntent = Intent(Intent.ACTION_SENDTO)
            emailIntent.data = Uri.parse("mailto:")
            emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))

            if (subject.isNotEmpty()) {
                emailIntent.putExtra(
                    Intent.EXTRA_SUBJECT,
                    subject
                )
            } else {
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
            }
            emailIntent.putExtra(Intent.EXTRA_TEXT, extraText)
            if (emailIntent.resolveActivity(activity.packageManager) != null) activity.startActivity(
                emailIntent
            )
        }

        fun copyToClipboard(baseActivity: ActBase<*>, text: String, title: String) {
            val clipboard =
                baseActivity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("Copied Text", text)
            clipboard.setPrimaryClip(clip)
            baseActivity.showSnackBar("$title copied to clipboard")
        }

        fun getOldPriceVisibility(price: Double, comparePrice: Double): Int {
            if (price < comparePrice) {
                return View.GONE
            }
            return View.VISIBLE
        }

        fun calculateNoOfColumns(context: Context): Int {
            val displayMetrics = context.resources.displayMetrics
            val dpWidth = displayMetrics.widthPixels / displayMetrics.density
            return (dpWidth / 180).toInt()
        }


        fun logW(TAG: String, msg: String) {
            if (BuildConfig.DEBUG) {
                if (BuildConfig.DEBUG) {
                    for (i in 0..msg.length / maxLogSize) {
                        val start = i * maxLogSize
                        var end = (i + 1) * maxLogSize
                        end = if (end > msg.length) msg.length else end
                        Log.w(TAG, msg.substring(start, end))
                    }
                }
            }
        }

        fun logI(TAG: String, msg: String) {
            if (BuildConfig.DEBUG) {
                for (i in 0..msg.length / maxLogSize) {
                    val start = i * maxLogSize
                    var end = (i + 1) * maxLogSize
                    end = if (end > msg.length) msg.length else end
                    Log.i(TAG, msg.substring(start, end))
                }
            }
        }

        fun getDp(px: Float): Float {
            val metrics = Resources.getSystem().displayMetrics
            val dp = px / (metrics.densityDpi / 160f)
            return Math.round(dp).toFloat()
        }

        fun getPx(dp: Float): Float {
            val metrics = Resources.getSystem().displayMetrics
            val px = dp * (metrics.densityDpi / 160f)
            log("getPx", "dp $dp px ${Math.round(px).toFloat()}")
            return Math.round(px).toFloat()
        }

        fun isMyServiceRunning(serviceClass: Class<*>, activity: Activity): Boolean {
            val manager = activity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.name == service.service.className) {
                    return true
                }
            }
            return false
        }


        fun setListViewHeightBasedOnChildren(listView: ListView) {
            val listAdapter = listView.adapter
                ?: // pre-condition
                return

            var totalHeight = 0
            for (i in 0 until listAdapter.count) {
                val listItem = listAdapter.getView(i, null, listView)
                listItem.measure(0, 0)
                totalHeight += listItem.measuredHeight
            }

            val params = listView.layoutParams
            params.height = totalHeight + listView.dividerHeight * (listAdapter.count - 1)
            listView.layoutParams = params
            listView.requestLayout()
        }

        fun isStringNull(str: String): String {
            if (str == null) {
                return ""
            }
            return str
        }

        fun convertToMyDateTime(oldDate: String): String? {
            val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
            inputFormat.timeZone = TimeZone.getTimeZone("UTC")
            val outputFormat = SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.getDefault())
            outputFormat.timeZone = TimeZone.getDefault()
            val date = inputFormat.parse(oldDate)

            val formattedDate = outputFormat.format(date)
            return formattedDate
        }

        fun convertToMyDateOnly(oldDate: String): String? {
            val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
            inputFormat.timeZone = TimeZone.getTimeZone("UTC")
            val outputFormat = SimpleDateFormat(dateOutPutPattern, Locale.getDefault())
            outputFormat.timeZone = TimeZone.getDefault()
            val date = inputFormat.parse(oldDate)

            val formattedDate = outputFormat.format(date)
            return formattedDate
        }

        fun convertToMyDate(oldDate: String?): String? {
            var formattedDate: String?
            try {
                val inputFormat = SimpleDateFormat(dateInputPattern, Locale.getDefault())
                val outputFormat = SimpleDateFormat(dateOutPutPattern, Locale.getDefault())
                val date = inputFormat.parse(oldDate!!)
                formattedDate = outputFormat.format(date!!)
            } catch (e: Exception) {
                e.printStackTrace()
                formattedDate = oldDate
            }
            return formattedDate
        }

        fun convertToMyDateUTC(oldDate: String?): String? {
            var formattedDate: String?
            try {
                val inputFormat = SimpleDateFormat(dateInputPattern, Locale.getDefault())
                inputFormat.timeZone = TimeZone.getTimeZone("UTC")
                val outputFormat = SimpleDateFormat(dateOutPutPattern, Locale.getDefault())
                outputFormat.timeZone = TimeZone.getDefault()
                val date = inputFormat.parse(oldDate!!)
                formattedDate = outputFormat.format(date!!)
            } catch (e: Exception) {
                e.printStackTrace()
                formattedDate = oldDate
            }
            return formattedDate
        }

        fun convertToMyTime(oldDate: String?): String? {//getting time into UTC timezone
            val formattedDate = try {
                val inputFormat = SimpleDateFormat(dateInputPattern, Locale.getDefault())
//                inputFormat.timeZone = TimeZone.getTimeZone("UTC") // removed because we didn't get utc time
                val outputFormat = SimpleDateFormat("hh:mm a", Locale.getDefault())
//                outputFormat.timeZone = TimeZone.getDefault()
                val date = inputFormat.parse(oldDate!!)
                outputFormat.format(date!!)
            } catch (e: Exception) {
                oldDate
            }
            return formattedDate
        }

        fun convertToMyTimeUTC(oldDate: String?): String? {//getting time into our timezone
            val formattedDate = try {
                val inputFormat = SimpleDateFormat(dateInputPattern, Locale.getDefault())
                inputFormat.timeZone =
                    TimeZone.getTimeZone("UTC") // removed because we didn't get utc time
                val outputFormat = SimpleDateFormat("hh:mm a", Locale.getDefault())
                outputFormat.timeZone = TimeZone.getDefault()
                val date = inputFormat.parse(oldDate!!)
                outputFormat.format(date!!)
            } catch (e: Exception) {
                oldDate
            }
            return formattedDate
        }

        fun getCurrentTimeIntoChatFormat(): String? {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val current = LocalDateTime.now()
                    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
                    var answer: String = current.format(formatter)
                    Log.d("answer", answer)
                    return answer
                } else {
                    var date = Date();
                    val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
                    val answer: String = formatter.format(date)
                    Log.d("answer", answer)
                    return answer
                }
            } catch (e: Exception) {
                return "";
            }
        }

        fun dateToTimeStamp(date: String?): Long {
            val inputFormat = SimpleDateFormat(dateInputPattern, Locale.getDefault())
//            inputFormat.timeZone = TimeZone.getTimeZone("UTC")
//            val outputFormat = SimpleDateFormat(dateInputPattern, Locale.getDefault())
//            outputFormat.timeZone = TimeZone.getDefault()
            return inputFormat.parse(date!!)!!.time
        }

        fun dateToTimeStampUTC(date: String?): Long {
            val inputFormat = SimpleDateFormat(dateInputPattern, Locale.getDefault())
            inputFormat.timeZone = TimeZone.getTimeZone("UTC")
            val outputFormat = SimpleDateFormat(dateInputPattern, Locale.getDefault())
            outputFormat.timeZone = TimeZone.getDefault()
            return outputFormat.parse(outputFormat.format(inputFormat.parse(date!!)!!))!!.time
        }

        // getting time for chat only
        fun convertChatTimeToMyTime(date: String?): String? {
            val formattedDate = try {
                val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                inputFormat.timeZone =
                    TimeZone.getTimeZone("UTC") // removed because we didn't get utc time
                val outputFormat = SimpleDateFormat("dd MMM yyyy HH:mm a", Locale.getDefault())
                outputFormat.timeZone = TimeZone.getDefault()
                val date = inputFormat.parse(date!!)
                outputFormat.format(date!!)
            } catch (e: Exception) {
                date
            }
            return formattedDate
        }

        fun timeAgo(messageTimestamp: Long): String {
            val xx = DateUtils.getRelativeTimeSpanString(
                messageTimestamp,
                System.currentTimeMillis(),
                DateUtils.SECOND_IN_MILLIS
            )
            return xx.toString()
        }

        fun formatted_date(timestamp: Long): String? {
            val cal = Calendar.getInstance()
            val tz = cal.timeZone //get your local time zone.
            val sdf = SimpleDateFormat("hh:mma", Locale.US) //dd MMM yyyy KK:mma
            sdf.timeZone = tz //set time zone.
            val localTime = sdf.format(Date(timestamp * 1000))
            return localTime.toLowerCase(Locale.getDefault())
        }

        fun getImageString(sourceString: String?, drawableImage: Int): Any {
            if (sourceString == null) {
                return drawableImage
            }
            return sourceString
        }

        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
        fun isValidContextForGlide(context: Context?): Boolean {
            if (context == null) {
                return false
            }
            if (context is Activity) {
                val activity = context
                if (activity.isDestroyed || activity.isFinishing) {
                    return false
                }
            }
            return true
        }

        fun addDays(days: Int): String? {
            val c = Calendar.getInstance()
            c.add(
                Calendar.DATE,
                days + 1
            )
            val sdf1 = SimpleDateFormat(dateOutPutPattern, Locale.getDefault())
            return sdf1.format(c.time)
        }

        fun removeDays(days: Int, date: String?): String? {
            val outputFormat = SimpleDateFormat(dateOutPutPattern, Locale.getDefault())
            outputFormat.parse(date.toString())
            val c = outputFormat.calendar
            c.add(
                Calendar.DATE,
                -days
            )
            val sdf1 = SimpleDateFormat(dateOutPutPattern, Locale.getDefault())
            return sdf1.format(c.time)
        }

        fun getDominantColor(dominantColor: String?): String? {
            if (TextUtils.isEmpty(dominantColor)) {
                return "#000000"
            }
            return dominantColor
        }

        fun makeTextViewResizable(
            tv: TextView,
            maxLine: Int,
            expandText: String,
            viewMore: Boolean,
            description: String?
        ) {
            if (tv.tag == null) {
                tv.tag = tv.text
            }
            val vto = tv.viewTreeObserver
            vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {

                override fun onGlobalLayout() {
                    val text: String
                    val lineEndIndex: Int
                    val obs = tv.viewTreeObserver
                    obs.removeOnGlobalLayoutListener(this)
                    try {
                        if (maxLine == 0) {
                            lineEndIndex = tv.layout.getLineEnd(0)
                            text = tv.text.subSequence(
                                0,
                                lineEndIndex - expandText.length + 1
                            ).toString() + " " + expandText
                        } else if (maxLine > 0 && tv.lineCount >= maxLine) {
                            lineEndIndex = tv.layout.getLineEnd(maxLine - 1)
                            text = tv.text.subSequence(
                                0,
                                lineEndIndex - expandText.length + 1
                            ).toString() + " " + expandText
                        } else {
                            lineEndIndex = tv.layout.getLineEnd(tv.layout.lineCount - 1)
                            text =
                                tv.text.subSequence(0, lineEndIndex).toString() + " " + expandText
                        }
                        tv.text = text
                        tv.movementMethod = LinkMovementMethod.getInstance()
                        tv.setText(
                            addClickablePartTextViewResizable(
                                Html.fromHtml(tv.text.toString()), tv, lineEndIndex, expandText,
                                viewMore, description
                            ), TextView.BufferType.SPANNABLE
                        )
                    } catch (e: Exception) {
                        log(TAG, "onGlobalLayout: crashed")
                        tv.text = description
                    }
                }
            })

        }

        private fun addClickablePartTextViewResizable(
            strSpanned: Spanned, tv: TextView,
            maxLine: Int, spanableText: String, viewMore: Boolean, description: String?
        ): SpannableStringBuilder {
            val str = strSpanned.toString()
            val ssb = SpannableStringBuilder(strSpanned)

            if (str.contains(spanableText)) {
                ssb.setSpan(object : ClickableSpan() {
                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds)
                        ds.isUnderlineText = false
                        //ds.linkColor = Color.TRANSPARENT
                    }

                    override fun onClick(widget: View) {
                        tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                        tv.invalidate()
                        if (viewMore) {
                            makeTextViewResizable(
                                tv,
                                -1,
                                " show less",
                                false,
                                description
                            )
                        } else {
                            makeTextViewResizable(
                                tv,
                                3,
                                " show more",
                                true,
                                description
                            )
                        }

                    }
                }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length, 0)


            }
            return ssb

        }

        fun isDeviceRooted(): Boolean {
            return checkRootMethod1() || checkRootMethod2() || checkRootMethod3()
        }

        private fun checkRootMethod1(): Boolean {
            val buildTags = android.os.Build.TAGS
            return buildTags != null && buildTags.contains("test-keys")
        }

        private fun checkRootMethod2(): Boolean {
            val paths = arrayOf(
                "/system/app/Superuser.apk",
                "/sbin/su",
                "/system/bin/su",
                "/system/xbin/su",
                "/data/local/xbin/su",
                "/data/local/bin/su",
                "/system/sd/xbin/su",
                "/system/bin/failsafe/su",
                "/data/local/su",
                "/su/bin/su"
            )
            for (path in paths) {
                if (File(path).exists()) return true
            }
            return false
        }

        private fun checkRootMethod3(): Boolean {
            var process: Process? = null
            try {
                process = Runtime.getRuntime().exec(arrayOf("/system/xbin/which", "su"))
                val line = BufferedReader(InputStreamReader(process!!.inputStream))
                return line.readLine() != null
            } catch (t: Throwable) {
                return false
            } finally {
                process?.destroy()
            }
        }

        fun checkSingleClick(mLastClickTime: Long): Boolean {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return true
            }

            return false
        }

        fun toRequestBody(value: String): RequestBody {
            return RequestBody.create("text/plain".toMediaType(), value)
        }

        fun toResponseBody(value: String): ResponseBody {
            return value.toResponseBody("text/plain".toMediaType())
        }

        fun getMimeType(url: String): String? {
            var type: String? = ""
            val extension = MimeTypeMap.getFileExtensionFromUrl(URLEncoder.encode(url, "utf-8"))
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            }
            return type
        }

        fun getExtension(url: String): String? {
            return MimeTypeMap.getFileExtensionFromUrl(url)
        }

        fun localToEmoji(first: String): String {
            val firstLetter: Int = Character.codePointAt(first, 0) - 0x41 + 0x1F1E6
            val secondLetter: Int = Character.codePointAt(first, 1) - 0x41 + 0x1F1E6
            return String(Character.toChars(firstLetter)) + String(Character.toChars(secondLetter))
        }

        fun longToInt(number: Long): String {
            return try {
                DecimalFormat("#").format(number)
            } catch (e: Exception) {
                number.toInt().toString()
            }
        }

        fun longToInt(number: Double): String {
            return try {
                DecimalFormat("#").format(number)
            } catch (e: Exception) {
                number.toInt().toString()
            }
        }

        fun convertToRequestHashMap(map: MutableMap<String, String>): MutableMap<String, RequestBody> {
            val requestMap: MutableMap<String, RequestBody> = mutableMapOf()
            for ((key, value) in map) {
                requestMap[key] = stringToRequestBody(value)
            }
            return requestMap
        }

        fun stringToRequestBody(string: String?): RequestBody {
            return if (string != null) {
                try {
                    string
                        .toRequestBody("text/plain".toMediaTypeOrNull())
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                    "".toRequestBody("text/plain".toMediaTypeOrNull())
                }
            } else {
                "".toRequestBody("text/plain".toMediaTypeOrNull())
            }
        }
    }
}

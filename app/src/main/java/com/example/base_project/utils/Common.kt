package com.example.base_project.utils

/**
 * Created by hlink21 on 27/4/16.
 */
object Common {
    const val SHARED_PREF_LANG = "application_lang"
    const val LANGUAGE_PREFERENCE = "languagePreference"
    val INDICATORS = arrayOf(
        "BallPulseIndicator",
        "BallGridPulseIndicator",
        "BallClipRotateIndicator",
        "BallClipRotatePulseIndicator",
        "SquareSpinIndicator",
        "BallClipRotateMultipleIndicator",
        "BallPulseRiseIndicator",
        "BallRotateIndicator",
        "CubeTransitionIndicator",
        "BallZigZagIndicator",
        "BallZigZagDeflectIndicator",
        "BallTrianglePathIndicator",
        "BallScaleIndicator",
        "LineScaleIndicator",
        "LineScalePartyIndicator",
        "BallScaleMultipleIndicator",
        "BallPulseSyncIndicator",
        "BallBeatIndicator",
        "LineScalePulseOutIndicator",
        "LineScalePulseOutRapidIndicator",
        "BallScaleRippleIndicator",
        "BallScaleRippleMultipleIndicator",
        "BallSpinFadeLoaderIndicator",
        "LineSpinFadeLoaderIndicator",
        "TriangleSkewSpinIndicator",
        "PacmanIndicator",
        "BallGridBeatIndicator",
        "SemiCircleSpinIndicator"
    )

    const val CURRENT_LATITUDE = "latitude"
    const val CURRENT_LONGITUDE = "longitude"
}

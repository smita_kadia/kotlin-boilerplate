package com.example.base_project.base

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.example.base_project.base.internet.AppExecutors
import com.example.base_project.base.internet.InternetStatusLiveData
import com.example.base_project.network.WebServiceClient


class MyApplication:Application() {

    lateinit var internetStatusLiveData: InternetStatusLiveData

    companion object {
        var BASE_URL  : String = "https://api.invupos.com/invuApiPos/"
        var COMMON_HEADER  : String = "bd_suvlascentralpos"
        var HANDLER_TIME  : Long = 3000

        lateinit  var appContext: Context
        val appExecutors: AppExecutors =
            AppExecutors()
    }



    override fun onCreate() {
        super.onCreate()

        // Initialize API Client
        WebServiceClient.init(this)
        appContext = applicationContext
        //Stetho.initializeWithDefaults(this)

        //FirebaseApp.initializeApp(this)
        //FirebaseMessaging.getInstance().isAutoInitEnabled = true
        MultiDex.install(this)

        internetStatusLiveData = InternetStatusLiveData(this)

    }


}
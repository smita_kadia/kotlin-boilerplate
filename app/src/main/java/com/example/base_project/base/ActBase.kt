package com.example.base_project.base

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.base_project.R
import com.example.base_project.base.internet.InternetStatusLiveData
import com.example.base_project.utils.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import retrofit2.HttpException
import java.io.IOException
import java.lang.Exception

open class ActBase<T : ViewDataBinding> : AppCompatActivity() {

    private val TAG = ActBase::class.java.simpleName

    protected lateinit var mActivity: AppCompatActivity

   // private lateinit var internetStatusLiveData: InternetStatusLiveData

    //    private var androidBug5497Workaround :AndroidBug5497Workaround? = null
    private var softInputAssist: SoftInputAssist? = null

    protected lateinit var mBinding: T


   // val session = object : AppSession
   // val session = object : AppPreferences(mActivity,)

   // protected  var session: Session = TODO()
   // protected  var helper: AppPreferences = TODO()

    //private var authViewModel: AuthenticationViewModel?
    private lateinit var progressDialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

       // internetStatusLiveData = (application as MyApplication).internetStatusLiveData


    }

    protected fun bindView(layoutId: Int) {

        mBinding = DataBindingUtil.setContentView(this, layoutId)

    }


    @Suppress("DEPRECATION")
    private fun setLightStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            val systemUiVisibilityFlags =
                window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.decorView.systemUiVisibility = systemUiVisibilityFlags
        }
    }


    private fun hideStatusBar() { // Hide status bar
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }


    fun showStatusBar() { // Hide status bar
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    fun getViewDataBinding(): T {
        return mBinding
    }

    /**
     * Override for set view model
     *
     * @return view model instance
     */


   /* fun isNetworkConnected(): Boolean {
        return internetStatusLiveData.isInternetAvailable()
    }*/


    fun showToast(msg: String) {
        Handler(Looper.getMainLooper())
            .post { Toast.makeText(mActivity, msg, Toast.LENGTH_LONG).show() }
    }

    fun showToast(@StringRes msg: Int) {
        Handler(Looper.getMainLooper())
            .post { Toast.makeText(mActivity, msg, Toast.LENGTH_LONG).show() }
    }

    fun showUToast(error1: MutableList<Error>) {
        val error = Gson().toJson(error1).toString()
//        Crashlytics.logException(Throwable(error))
        Handler(Looper.getMainLooper())
            .post {
                Toast.makeText(
                    mActivity,
                    "mActivity.resources.getString(R.string.msg_unexpected_error)",
                    Toast.LENGTH_SHORT
                ).show()
            }
    }

    fun shareTextContent(shareText: String) {
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareText)
        startActivity(Intent.createChooser(shareIntent, "Send to..."))
    }

    fun showError() {
        Handler(Looper.getMainLooper())
            .post {
                Toast.makeText(
                    mActivity,
                    "mActivity.resources.getString(R.string.msg_internet_connection)",
                    Toast.LENGTH_LONG
                ).show()
            }
    }

    fun serverError(throwable: HttpException) {
        hideProgressDialog()
        showToast("Server is in maintenance, please try again later")
//        Crashlytics.logException(Throwable(Gson().toJson(throwable)))
    }

    fun authError(throwable: Throwable?) {
//        pd.hide()
        hideProgressDialog()
       // session.sessionClear()
        //session.sessionLogOut()
        Utils.log(TAG, "authError: " + throwable?.localizedMessage)
        exitTheApp()

    }


    fun exitTheApp() {

        showProgressDialog()
        Thread {
            try {
                /*FirebaseMessaging.getInstance().unsubscribeFromTopic(AppConfig.EXTRA.FCM_GROUP)
                FirebaseInstanceId.getInstance().deleteInstanceId()*/
                //helper.clearAll()
               /* runOnUiThread {
                    hideProgressDialog()
                    this@BaseActivity.launchActivity<LoginActivity> {
                    }
                    finish()
                }*/
            } catch (e: IOException) {
                runOnUiThread {
                    showToast("Logout Error")
                    hideProgressDialog()
                }
                Utils.log(TAG, "logout: " + e.localizedMessage)
            }
        }.start()
    }

    /**
     * show progress Dialog for application
     */
    open fun showProgressDialog() {
        hideProgressDialog()
        ProgressAlertUtils.showCustomProgressDialog(this)
    }

    /**
     * Hide progress Dialog for whole application
     */
    open fun hideProgressDialog() {
        ProgressAlertUtils.dismissDialog()
    }

     fun showSnackBar(message: String) {
        showSnackBar(message, false)
    }

     fun showSnackBar(message: String, showOk: Boolean) {
        showSnackBar(mBinding.root, message, showOk)
    }

     fun showSnackBar(view: View, message: String, showOk: Boolean) {
        val snackBar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
        snackBar.setActionTextColor(ContextCompat.getColor(this, R.color.colorPrimary))

        val sbView = snackBar.view
        sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        val textView = sbView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(this, R.color.white))
        //textView.typeface = ResourcesCompat.getFont(this, R.font.montserrat)

        if (showOk)
            snackBar.setAction("Ok") { snackBar.dismiss() }

        snackBar.show()
    }

     fun showErrorSnackBar() {
        val snackBar = Snackbar.make(
            mBinding.root,
            R.string.str_msg_no_internet,
            Snackbar.LENGTH_LONG
        )
        snackBar.setActionTextColor(ContextCompat.getColor(this, R.color.colorPrimary))

        val sbView = snackBar.view
        sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        val textView = sbView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
        //textView.typeface = ResourcesCompat.getFont(this, R.font.montserrat)

        snackBar.show()
    }

     fun showLoader() {
        hideLoader()
        ProgressAlertUtils.showCustomProgressDialog(this)
    }

     fun hideLoader() {
        ProgressAlertUtils.dismissDialog()
    }


     fun showKeyBoard() {
        val view = this.currentFocus
        if (view != null) {
            val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

     fun hideKeyBoard() {
        val view = this.currentFocus
        if (view != null) {
            val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                view.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }

     fun showDialogWithOneAction(
        title: String?, message: String?,
        positiveButton: String?,
        positiveFunction: (DialogInterface, Int) -> Unit
    ) {
        showDialogWithTwoActions(title, message, positiveButton, null, positiveFunction) { _, _ -> }
    }

    private var materialAlertDialog: AlertDialog? = null
     fun showDialogWithTwoActions(
        title: String?, message: String?,
        positiveName: String?, negativeName: String?,
        positiveFunction: (DialogInterface, Int) -> Unit,
        negativeFunction: (DialogInterface, Int) -> Unit
    ) {

        materialAlertDialog?.dismiss()
        materialAlertDialog = MaterialAlertDialogBuilder(this).setCancelable(false).setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveName, positiveFunction)
            .setNegativeButton(negativeName, negativeFunction).create()
        materialAlertDialog?.show()

        val textView = materialAlertDialog?.findViewById<TextView>(android.R.id.message)
        if (textView != null) {
            //textView.typeface = ResourcesCompat.getFont(this, R.font.montserrat)
        }
    }

     fun hideDialog() {
        if (materialAlertDialog?.isShowing!!) {
            materialAlertDialog?.dismiss()
        }
    }


/*
    fun inflateNoDataView(layout: FrameLayout, message: String = "") {
        layout.removeAllViews()
        val inflater: LayoutInflater =
            LayoutInflater.from(this).context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val mBinding: LayoutNoDataBinding =
            DataBindingUtil.inflate(
                inflater, R.layout.layout_no_data, null, false
            )

        mBinding.model = session.getUser()
        mBinding.tvMessage.text =
            if (message.isNullOrEmpty()) getString(R.string.you_don_t_have_any_assigned_task_yet) else message

        layout.addView(mBinding.root)
    }*/

    fun openDatePicker(
        title: String,
        textView: TextView
    ) {
        DatePickerHelper(
            supportFragmentManager,
            title,
            textView
        ).show()
    }

    inline fun <reified T : Any> FragmentActivity.launchActivity(
        options: Bundle? = null,
        noinline init: Intent.() -> Unit = {}
    ) {
        val intent = newIntent<T>(this)
        intent.init()
        startActivityForResult(intent, 7777, options)
    }

    inline fun <reified T : Any> Activity.launchActivity(
        options: Bundle? = null,
        noinline init: Intent.() -> Unit = {}
    ) {
        val intent = newIntent<T>(this)
        intent.init()
        startActivityForResult(intent, 7777, options)
    }

    inline fun <reified T : Any> Activity.launchActivity(
        requestCode: Int,
        options: Bundle? = null,
        noinline init: Intent.() -> Unit = {}
    ) {
        val intent = newIntent<T>(this)
        intent.init()
        startActivityForResult(intent, requestCode, options)
    }

    inline fun <reified T : Any> newIntent(context: Context): Intent =
        Intent(context, T::class.java)

    var isResultSuccess = false

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == 7777 && resultCode == RESULT_OK) {
//            isResultSuccess = true
           // refreshApiCallOnResultSuccess()
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    fun setResultWithOnBackPressed() {
        setResult(RESULT_OK)
        finish()
    }

    fun setResultWithOnBackPressed(intent: Intent) {
        setResult(RESULT_OK, intent)
        finish()
    }

    override fun onBackPressed() {
//        if (isResultSuccess) {
        setResult(RESULT_OK)
//        }
        finish()
    }

    fun recreateActivity() {
        startActivity(intent)
        finish()
        overridePendingTransition(0, 0)
    }


    override fun onResume() {
        super.onResume()
        val filter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
//        androidBug5497Workaround?.addListener()
        softInputAssist?.onResume()
    }

    override fun onPause() {
        super.onPause()

        softInputAssist?.onPause()
//        androidBug5497Workaround?.removeListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        softInputAssist?.onDestroy()
    }

    private val MIN_CLICK_INTERVAL: Long = 2000 //in millis

    private var lastClickTime: Long = 0

    fun checkForSafeClick(): Boolean {
        val currentTime: Long = SystemClock.elapsedRealtime()
        return if ((currentTime - lastClickTime) >= MIN_CLICK_INTERVAL) {
            lastClickTime = currentTime
            true
        } else {
            lastClickTime = currentTime
            false
        }

    }

    val FRAGMENT_JUST_REPLACE = 0
    val FRAGMENT_JUST_ADD = 1
    val FRAGMENT_ADD_TO_BACKSTACK_AND_REPLACE = 2
    val FRAGMENT_ADD_TO_BACKSTACK_AND_ADD = 3

    open fun pushFragmentIgnoreCurrent(fragment: Fragment, fragmentTransactionType: Int) {
        when (fragmentTransactionType) {
            FRAGMENT_JUST_REPLACE -> pushFragments(
                R.id.fragment_container,
                null,
                fragment,
                false,
                false,
                true,
                false
            )
            FRAGMENT_JUST_ADD -> pushFragments(
                R.id.fragment_container,
                null,
                fragment,
                true,
                false,
                true,
                true
            )
            FRAGMENT_ADD_TO_BACKSTACK_AND_REPLACE -> pushFragments(
                R.id.fragment_container,
                null,
                fragment,
                true,
                true,
                true,
                false
            )
            FRAGMENT_ADD_TO_BACKSTACK_AND_ADD -> pushFragments(
                R.id.fragment_container,
                null,
                fragment,
                true,
                true,
                true,
                true
            )
        }
    }

    open fun pushFragmentDontIgnoreCurrent(fragment: Fragment?, fragmentTransactionType: Int) {
        when (fragmentTransactionType) {
            FRAGMENT_JUST_REPLACE -> pushFragments(
                R.id.fragment_container,
                null,
                fragment!!,
                true,
                false,
                false,
                false
            )
            FRAGMENT_JUST_ADD -> pushFragments(
                R.id.fragment_container,
                null,
                fragment!!,
                true,
                false,
                false,
                true
            )
            FRAGMENT_ADD_TO_BACKSTACK_AND_REPLACE -> pushFragments(
                R.id.fragment_container,
                null,
                fragment!!,
                true,
                true,
                false,
                false
            )
            FRAGMENT_ADD_TO_BACKSTACK_AND_ADD -> pushFragments(
                R.id.fragment_container,
                null,
                fragment!!,
                true,
                true,
                false,
                true
            )
        }
    }

    open fun pushFragments(
        id: Int, args: Bundle?,
        fragment: Fragment, shouldAnimate: Boolean,
        shouldAdd: Boolean, ignoreIfCurrent: Boolean, justAdd: Boolean,
    ) {
        println("fragment NAME>>>>>" + fragment.javaClass.name)
        pushFragments(id, args, fragment, null, shouldAnimate, shouldAdd, ignoreIfCurrent, justAdd)
    }

    open fun pushFragments(
        id: Int, args: Bundle?,
        fragment: Fragment?, fragmentParent: Fragment?, shouldAnimate: Boolean,
        shouldAdd: Boolean, ignoreIfCurrent: Boolean, justAdd: Boolean,
    ) {
        try {
            val i = 0
            hideKeyBoard()
            val currentFragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
            if (ignoreIfCurrent && currentFragment != null && fragment != null && currentFragment.javaClass == fragment.javaClass) {
                return
            }

            // assert fragment != null;
            if (fragment!!.arguments == null) {
                fragment!!.arguments = args
            }
            val fragmentManager = supportFragmentManager

            /* if (fragmentParent == null) {

            } else {
                fragmentManager = fragmentParent.getChildFragmentManager();
            }*/
            val ft = fragmentManager.beginTransaction()
            if (shouldAdd) {
                //  ft.addToBackStack(null);
                ft.addToBackStack(fragment!!.javaClass.canonicalName + (i + 1))
                //ft.add(id, fragment, fragment.getClass().getCanonicalName()+(i+1));
            }
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            if (shouldAnimate) {
                //System.out.println("You are doing animations :::----");
                //ft.setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_right,R.animator.slide_out_right, R.animator.slide_out_right);
                /*ft.setCustomAnimations(
                        R.animator.anim_slide_in_left, R.animator.anim_slide_in_right,
                        R.animator.anim_slide_in_left1, R.animator.anim_slide_in_right1);*/
            } else {
                //System.out.println("You are NOT doing animations :::----");
            }


            // if(fragmentLast!=null)ft.hide(fragmentLast);
            if (justAdd) {
                //System.out.println("if");
                removeIfExists(fragmentManager, fragment)

                //ft.add(id, fragment, fragment.getClass().getCanonicalName()+(i+1));
                ft.add(id, fragment!!, fragment!!.javaClass.canonicalName + (i + 1))
                if (supportFragmentManager.findFragmentById(R.id.fragment_container) != null) {
                    // Fragment fragmentNew = context.getSupportFragmentManager().findFragmentById(R.id.container);
                    ft.hide(supportFragmentManager.findFragmentById(R.id.fragment_container)!!)
                }
            } else {
                //System.out.println("else");
                //clearBackStack();
                ft.replace(R.id.fragment_container, fragment!!)
                //ft.replace(id, fragment, fragment.getClass().getCanonicalName());
            }
            ft.commitAllowingStateLoss()
            //ft.commit();
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    open fun removeIfExists(fragmentManager: FragmentManager, fragment: Fragment) {
        val f = fragmentManager.findFragmentByTag(fragment.javaClass.canonicalName)
        if (f != null && f.javaClass == fragment.javaClass) {
            fragmentManager.beginTransaction().remove(f).commit()
            fragmentManager.popBackStack()
        }
    }


}
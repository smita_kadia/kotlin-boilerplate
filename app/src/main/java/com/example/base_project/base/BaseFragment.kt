package com.example.base_project.base

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.example.base_project.R
import com.example.base_project.base.internet.InternetStatus
import com.example.base_project.base.internet.InternetStatusLiveData
import com.example.base_project.databinding.LayoutNoInternetBinding
import com.example.base_project.ui.activity.ActLogin
import com.example.base_project.utils.Utils
import com.example.base_project.utils.helper.AlertUtil
import com.example.base_project.utils.helper.LiveDataUtil

import java.util.*


abstract class BaseFragment : Fragment() {

    lateinit var internetStatusLiveData: InternetStatusLiveData

    private lateinit var majorError: LiveData<String>

    private val refreshLiveData: MutableLiveData<Boolean> = MutableLiveData<Boolean>()

    private var autoRefresh = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        internetStatusLiveData = (activity?.application as MyApplication).internetStatusLiveData

       // setLanguage()
    }


    fun internetIsAvailable(): Boolean {

        return if (internetStatusLiveData.value == null) {

            false

        } else {

            val status = internetStatusLiveData.value as InternetStatus

            val connected = status.isConnected

            if (!connected) {

                updateInternetLayout(status)
            }

            connected
        }
    }

    open fun checkGps() {
        val mLocationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val mGPS = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if (!mGPS) {
            AlertUtil.showAlert(requireContext(),
                getString(R.string.str_please_turn_on_your_location),
                null,
                object : AlertUtil.OkClick {
                    override fun onClicked() {


                    }
                })
        }
    }

    fun connectedNetworkType(): Int {

        if (internetStatusLiveData.value == null) return InternetStatus.Type.NONE

        val status = internetStatusLiveData.value as InternetStatus

        return status.connectionType
    }

    fun setMajorError(majorError: LiveData<String>) {

        this.majorError = majorError

        this.majorError.observe(this, Observer {

            run {

            }
        })
    }

    fun forceLogOut(it : String){
        AlertUtil.showAlert(requireContext(),
            it,
            null,
            object : AlertUtil.OkClick {
                override fun onClicked() {
                    logoutFromEverything()
                }
            })
    }

    private fun logoutFromEverything() {
       // UserModel.logout(requireActivity().application)

      //  ApiClient.logout()

//        LoginManager.getInstance().logOut()


//        googleSignInHelper.logOut()

        val intent = Intent(requireContext(), ActLogin::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)

    }

    fun observeInternet(lifecycleOwner: LifecycleOwner) {

        internetStatusLiveData.observe(lifecycleOwner, Observer { status ->

            run {

                if (autoRefresh)

                    updateInternetLayout(status)
            }
        })
    }

    fun updateInternetLayout(status: InternetStatus) {

        val viewGroup = (view as ViewGroup)

        if (!status.isConnected) {

            Utils.log("Child count ${viewGroup.childCount}","data")

            if (viewGroup.childCount > 0) {

                Utils.log("Child ${viewGroup.getChildAt(0)}","data")

                viewGroup.getChildAt(0).visibility = View.GONE

                if (viewGroup.childCount > 1) return

                val noInternetBinding: LayoutNoInternetBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.layout_no_internet, viewGroup, false
                )

                noInternetBinding.btnTryAgain.setOnClickListener { v ->

                    run {

                        if (!internetStatusLiveData.tryAgain()) {

                            Toast.makeText(context, "No internet!", Toast.LENGTH_LONG).show()
                        }

                        updateInternetLayout(LiveDataUtil.safeUnbox(internetStatusLiveData))
                    }
                }

                viewGroup.addView(noInternetBinding.root)
            }

            refreshLiveData.value = false

        } else {

            var i = 0

            Utils.log("Child count ${viewGroup.childCount}","data")

            for (v in viewGroup.children) {

                Utils.log("Child count ${viewGroup.getChildAt(i)}","data")

                if (i == 0) {

                    viewGroup.getChildAt(i).visibility = View.VISIBLE

                } else {

                    viewGroup.removeViewAt(i)
                }

                i++
            }

            refreshLiveData.value = true
        }
    }

    fun setAutoRefresh(autoRefresh: Boolean) {
        this.autoRefresh = autoRefresh
    }

    fun getRefreshLiveData(): LiveData<Boolean> {

        return refreshLiveData
    }

    override fun onResume() {
        super.onResume()
        //setLanguage()
    }


   /* fun setLanguage() {

        val applicationRes = activity?.application?.applicationContext?.resources
        val res = resources
        val dm = resources.displayMetrics
       // val userLocale = Locale(UserModel.retrieveLanguagePref(requireActivity().application), "US")

        Locale.setDefault(userLocale) // Set for Locale
        val conf = Configuration(resources.configuration)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            conf.setLocale(userLocale)
        }

        res.updateConfiguration(conf, dm)
        applicationRes?.updateConfiguration(conf, dm)
    }*/

}
package com.example.base_project.ui.activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.base_project.R
import com.example.base_project.base.ActBase
import com.example.base_project.databinding.ActForgotPasswordBinding
import com.example.base_project.model.forgot_password.ForgotPasswordInputModel
import com.example.base_project.base.MyApplication
import com.example.base_project.viewmodel.ForgotPasswordViewModel
import java.util.*

class ActForgotPassword :  ActBase<ActForgotPasswordBinding>() {

    private lateinit var forgotpasswordviewmodel: ForgotPasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindView(R.layout.act_forgot_password)
        init()
    }

    /*intialize setup for this activity*/
    private fun init() {
        forgotpasswordviewmodel = ViewModelProvider(this).get(ForgotPasswordViewModel::class.java)
        mBinding.forgotPasswordViewModel = forgotpasswordviewmodel


        forgotpasswordviewmodel.getData()!!.observe(this, object : Observer<ForgotPasswordInputModel?> {
            override fun onChanged(user: ForgotPasswordInputModel?) {
                if (TextUtils.isEmpty(Objects.requireNonNull(user)?.strEmailAddress)) {
                    mBinding.edtUserNameEmail.error = getString(R.string.str_val_enter_email);
                    mBinding.edtUserNameEmail.requestFocus()
                } else if (!user!!.isEmailValid) {
                    mBinding.edtUserNameEmail.error  = getString(R.string.str_val_enter_email_valid)
                    mBinding.edtUserNameEmail.requestFocus()
                }  else {
                    /*call api here*/
                    hideKeyBoard()

                    showSnackBar(getString(R.string.str_forgot_password_desc))

                    Handler(Looper.getMainLooper()).postDelayed({
                        val intent = Intent(
                            this@ActForgotPassword,
                            ActLogin::class.java
                        )
                        intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
                        startActivity(intent)
                        finish()
                        // Your Code
                    }, MyApplication.HANDLER_TIME)



                }
            }
        })

    }
}
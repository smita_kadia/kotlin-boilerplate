package com.example.base_project.ui.activity

import LoginInputModel
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.base_project.R
import com.example.base_project.base.ActBase
import com.example.base_project.databinding.ActLoginBinding
import com.example.base_project.base.MyApplication
import com.example.base_project.viewmodel.LoginViewModel
import java.util.*


class ActLogin :   ActBase<ActLoginBinding>() {
    private lateinit var loginviewmodel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindView(R.layout.act_login)
        init()
    }

 /*intialize setup for this activity*/
    private fun init() {
        loginviewmodel = ViewModelProvider(this).get(LoginViewModel::class.java)
        mBinding.loginViewModel = loginviewmodel


        loginviewmodel.getUser()!!.observe(this, object : Observer<LoginInputModel?> {
            override fun onChanged(user: LoginInputModel?) {
                if (TextUtils.isEmpty(Objects.requireNonNull(user)?.strEmailAddress)) {
                    mBinding.edtUserNameEmail.error = getString(R.string.str_val_enter_email);
                    mBinding.edtUserNameEmail.requestFocus()
                } else if (!user!!.isEmailValid) {
                    mBinding.edtUserNameEmail.error  = getString(R.string.str_val_enter_email_valid)
                    mBinding.edtUserNameEmail.requestFocus()
                } else if (TextUtils.isEmpty(Objects.requireNonNull(user)?.strPassword)) {
                    mBinding.edtPassword.error = getString(R.string.str_val_enter_password )
                    mBinding.edtPassword.requestFocus()
                } else if (!user.isPasswordLengthGreaterThan5) {
                    mBinding.edtPassword.error = getString(R.string.str_val_enter_password_val)
                    mBinding.edtPassword.requestFocus()
                } else {
                   /*call api here*/

                   hideKeyBoard()
                   showSnackBar(getString(R.string.str_login_sucess))

                    Handler(Looper.getMainLooper()).postDelayed({
                        val intent = Intent(
                            this@ActLogin,
                            ActMain::class.java
                        )
                        intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
                        startActivity(intent)
                        finish()
                        // Your Code
                    }, MyApplication.HANDLER_TIME)
                }
            }
        })

    }



}
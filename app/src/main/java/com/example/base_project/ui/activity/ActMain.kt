package com.example.base_project.ui.activity

import android.os.Bundle
import com.example.base_project.R
import com.example.base_project.base.ActBase
import com.example.base_project.databinding.ActMainBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView


class ActMain :  ActBase<ActMainBinding>()  {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindView(R.layout.act_main)
        init()
    }

    /*intialize setup for this activity*/
    private fun init() {
        mBinding.bottomNavView.setOnItemSelectedListener(NavigationBarView.OnItemSelectedListener { item ->
            val id = item.itemId
            when (id) {
            R.id.menu_home ->{}
            R.id.menu_about ->{}
            R.id.menu_notification ->{}
            R.id.menu_settings ->{}
            }
            true
        })


    }


}
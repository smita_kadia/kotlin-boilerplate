package com.example.base_project.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.base_project.R
import com.example.base_project.base.ActBase
import com.example.base_project.databinding.ActSignUpBinding
import com.example.base_project.model.signup.SignUpInputModel
import com.example.base_project.base.MyApplication
import com.example.base_project.viewmodel.SignUpViewModel
import java.util.*

class ActSignUP :  ActBase<ActSignUpBinding>() {

    private lateinit var signUpViewModel : SignUpViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindView(R.layout.act_sign_up)
        init()
    }

    /*intialize setup for this activity*/
    private fun init() {
        signUpViewModel = ViewModelProvider(this).get(SignUpViewModel::class.java)
        mBinding.signUpViewmodel = signUpViewModel


        signUpViewModel.getUser()!!.observe(this, object : Observer<SignUpInputModel?> {
            override fun onChanged(user: SignUpInputModel?) {
                if (TextUtils.isEmpty(Objects.requireNonNull(user)?.name)) {
                    mBinding.edtName.error = getString(R.string.str_val_enter_name);
                    mBinding.edtName.requestFocus()
                } else if (TextUtils.isEmpty(Objects.requireNonNull(user)?.strEmailAddress)) {
                    mBinding.edtUserNameEmail.error = getString(R.string.str_val_enter_email);
                    mBinding.edtUserNameEmail.requestFocus()
                } else if (!user!!.isEmailValid) {
                    mBinding.edtUserNameEmail.error  = getString(R.string.str_val_enter_email_valid)
                    mBinding.edtUserNameEmail.requestFocus()
                }else if (TextUtils.isEmpty(Objects.requireNonNull(user)?.strPhone)) {
                    mBinding.edtphone.error = getString(R.string.str_val_enter_phone_number )
                    mBinding.edtphone.requestFocus()
                } else if (!user.isPhoneLengthGreaterThan10) {
                    mBinding.edtphone.error = getString(R.string.str_val_enter_valid_phone_number)
                    mBinding.edtphone.requestFocus()
                } else if (TextUtils.isEmpty(Objects.requireNonNull(user)?.strPassword)) {
                    mBinding.edtPassword.error = getString(R.string.str_val_enter_password )
                    mBinding.edtPassword.requestFocus()
                } else if (!user.isPasswordLengthGreaterThan5) {
                    mBinding.edtPassword.error = getString(R.string.str_val_enter_password_val)
                    mBinding.edtPassword.requestFocus()
                } else {
                    /*call api here*/

                    hideKeyBoard()
                    showSnackBar(getString(R.string.str_register_sucess))

                    Handler(Looper.getMainLooper()).postDelayed({
                        val intent = Intent(
                            this@ActSignUP,
                            ActMain::class.java
                        )
                        intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
                        startActivity(intent)
                        finish()
                        // Your Code
                    }, MyApplication.HANDLER_TIME)
                }
            }
        })

    }
}
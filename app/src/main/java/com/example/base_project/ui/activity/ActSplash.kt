package com.example.base_project.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.example.base_project.R
import com.example.base_project.base.ActBase
import com.example.base_project.databinding.ActSplashBinding
import com.example.base_project.base.MyApplication.Companion.HANDLER_TIME


class ActSplash :  ActBase<ActSplashBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindView(R.layout.act_splash)

        openMainScreen()
    }

    private fun openMainScreen() {
        Handler(Looper.getMainLooper()).postDelayed({
            val intent = Intent(
                this@ActSplash,
                ActLogin::class.java
            )
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(intent)
            finish()
            // Your Code
        }, HANDLER_TIME)

    }
}
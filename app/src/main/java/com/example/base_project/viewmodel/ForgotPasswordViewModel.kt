package com.example.base_project.viewmodel

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.base_project.model.forgot_password.ForgotPasswordInputModel
import com.example.base_project.ui.activity.ActForgotPassword
import com.example.base_project.ui.activity.ActLogin

class ForgotPasswordViewModel : ViewModel()  {
    var EmailAddress = MutableLiveData<String>()

    private var userMutableLiveData: MutableLiveData<ForgotPasswordInputModel>? = null

    fun getData(): MutableLiveData<ForgotPasswordInputModel>? {
        if (userMutableLiveData == null) {
            userMutableLiveData = MutableLiveData<ForgotPasswordInputModel>()
        }
        return userMutableLiveData
    }

    fun onClick(view: View?) {
        val loginUser = ForgotPasswordInputModel(EmailAddress.value)
        userMutableLiveData!!.setValue(loginUser)


    }





}
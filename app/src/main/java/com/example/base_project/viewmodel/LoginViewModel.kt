package com.example.base_project.viewmodel

import LoginInputModel
import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.base_project.ui.activity.ActForgotPassword
import com.example.base_project.ui.activity.ActLogin
import com.example.base_project.ui.activity.ActSignUP


class LoginViewModel : ViewModel()  {
    var EmailAddress = MutableLiveData<String>()
    var Password = MutableLiveData<String>()

    private var userMutableLiveData: MutableLiveData<LoginInputModel>? = null

    fun getUser(): MutableLiveData<LoginInputModel>? {
        if (userMutableLiveData == null) {
            userMutableLiveData = MutableLiveData<LoginInputModel>()
        }
        return userMutableLiveData
    }

    fun onClick(view: View?) {
        val loginUser = LoginInputModel(EmailAddress.value, Password.value)
        userMutableLiveData!!.setValue(loginUser)
    }

    fun onForgotClick(view: View?) {
        val context: Context = view!!.context
        val intent = Intent(
            context,
            ActForgotPassword::class.java
        )
        intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
        context.startActivity(intent)

    }

    fun onSignUpClick(view: View?) {
        val context: Context = view!!.context
        val intent = Intent(
            context,
            ActSignUP::class.java
        )
        intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
        context.startActivity(intent)

    }



}
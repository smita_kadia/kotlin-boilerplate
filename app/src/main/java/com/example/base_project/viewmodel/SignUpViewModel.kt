package com.example.base_project.viewmodel

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.base_project.model.forgot_password.ForgotPasswordInputModel
import com.example.base_project.model.signup.SignUpInputModel

class SignUpViewModel : ViewModel()  {
    var EmailAddress = MutableLiveData<String>()
    var Password = MutableLiveData<String>()
    var name = MutableLiveData<String>()
    var phone = MutableLiveData<String>()

    private var userMutableLiveData: MutableLiveData<SignUpInputModel>? = null

    fun getUser(): MutableLiveData<SignUpInputModel>? {
        if (userMutableLiveData == null) {
            userMutableLiveData = MutableLiveData<SignUpInputModel>()
        }
        return userMutableLiveData
    }

    fun onClick(view: View?) {
        val loginUser = SignUpInputModel(name.value,EmailAddress.value,phone.value, Password.value)
        userMutableLiveData!!.setValue(loginUser)
    }





}
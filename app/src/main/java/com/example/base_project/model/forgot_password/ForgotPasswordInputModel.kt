package com.example.base_project.model.forgot_password

import android.util.Patterns

class ForgotPasswordInputModel (val strEmailAddress: String?) {

    val isEmailValid: Boolean
        get() = Patterns.EMAIL_ADDRESS.matcher(strEmailAddress).matches()

}
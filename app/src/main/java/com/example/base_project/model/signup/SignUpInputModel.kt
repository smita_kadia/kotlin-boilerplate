package com.example.base_project.model.signup

import android.util.Patterns

class SignUpInputModel (val name: String?,val strEmailAddress: String?,val strPhone: String?, val strPassword: String?) {
    val isEmailValid: Boolean
        get() = Patterns.EMAIL_ADDRESS.matcher(strEmailAddress).matches()
    val isPasswordLengthGreaterThan5: Boolean
        get() = strPassword!!.length > 5
    val isPhoneLengthGreaterThan10: Boolean
        get() = strPhone!!.length > 10

}
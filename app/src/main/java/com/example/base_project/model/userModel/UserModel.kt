package com.example.base_project.model.userModel

import com.example.base_project.utils.extensions.isNotNullOrEmpty
import com.google.gson.annotations.SerializedName

data class UserModel(

    @SerializedName("pk") val pk: Int = 0,
    @SerializedName("username") val username: String = "",
    @SerializedName("first_name") val first_name: String = "",
    @SerializedName("last_name") val last_name: String? = "",
    @SerializedName("email") val email: String = "",
    @SerializedName("avatar") val avatar: String = "",
    @SerializedName("contact_number") val contact_number: String? = "",
    @SerializedName("is_active") val is_active: Boolean = false,
    @SerializedName("is_superuser") val is_superuser: Boolean = false,
    @SerializedName("place") val place: String = "",
    @SerializedName("address_line_1") val address_line_1: String = "",
    @SerializedName("address_line_2") val address_line_2: String = "",
    @SerializedName("city") val city: String = "",
    @SerializedName("state") val state: String = "",
    @SerializedName("country") val country: String = "",
    @SerializedName("latitude") val latitude: Double = 0.0,
    @SerializedName("longitude") val longitude: Double = 0.0,
    @SerializedName("zipcode") val zipcode: String = "",
    @SerializedName("full_address") val full_address: String = "",
    @SerializedName("date_joined") val date_joined: String = "",
    @SerializedName("is_assigned") val is_assigned: Boolean = false,
    @SerializedName("user_type") val user_type: List<String> = listOf(),
    @SerializedName("country_code") val country_code: String? = ""
) {
    fun getFullName(): String {
        return "$first_name ${if (last_name.isNotNullOrEmpty()) last_name else ""}"
    }

    fun getContactNumberWithCountryCode(): String {
        return "($country_code) $contact_number"
    }

    fun isCountryCodeAndContactNumberNotNull(): Boolean {
        return country_code.isNotNullOrEmpty() && contact_number.isNotNullOrEmpty()
    }
}
package com.example.base_project.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SubCategoryModel  : CommonResponseModel() {


    @SerializedName("data")
    @Expose
    var data: MutableList<Category>? = null


    class Category {

        @SerializedName("modificadores")
        @Expose
        var modificadores: List<Any>? = null

        @SerializedName("idmenu")
        @Expose
        var idmenu: String? = null

        @SerializedName("precioSugerido")
        @Expose
        var precioSugerido: String? = null

        @SerializedName("nombre")
        @Expose
        var nombre: String? = null

        @SerializedName("imagen")
        @Expose
        var imagen: String? = null

        @SerializedName("codigo")
        @Expose
        var codigo: String? = null

        @SerializedName("impuesto")
        @Expose
        var impuesto: Int? = null

        @SerializedName("codigoBarra")
        @Expose
        var codigoBarra: String? = null

        @SerializedName("precio_abierto")
        @Expose
        var precioAbierto: Boolean? = null

        @SerializedName("comision")
        @Expose
        var comision: String? = null

        @SerializedName("tipo_comision")
        @Expose
        var tipoComision: String? = null

        @SerializedName("descTipoComision")
        @Expose
        var descTipoComision: String? = null

        @SerializedName("impuestoAplicado")
        @Expose
        var impuestoAplicado: Boolean? = null

        @SerializedName("tipo")
        @Expose
        var tipo: String? = null

        @SerializedName("tipo_desc")
        @Expose
        var tipoDesc: String? = null

        @SerializedName("descripcion")
        @Expose
        var descripcion: Any? = null

        @SerializedName("permite_descuentos")
        @Expose
        var permiteDescuentos: Boolean? = null

        @SerializedName("categoria")
        @Expose
        var categoria: Categoria? = null


    }

  class Categoria{
      @SerializedName("idcategoriamenu")
      @Expose
      var idcategoriamenu: String? = null

      @SerializedName("nombremenu")
      @Expose
      var nombremenu: String? = null

      @SerializedName("porcentaje")
      @Expose
      var porcentaje: String? = null

      @SerializedName("impuesto")
      @Expose
      var impuesto: String? = null

      @SerializedName("codigo")
      @Expose
      var codigo: String? = null

      @SerializedName("orden")
      @Expose
      var orden: String? = null

      @SerializedName("printers")
      @Expose
      var printers: List<Any>? = null
  }
}

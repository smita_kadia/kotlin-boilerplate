package com.example.base_project.model.userModel

import com.google.gson.annotations.SerializedName

data class UserModelWithToken(
    @SerializedName("expiry") val expiry: String = "",
    @SerializedName("token") val token: String = "",
    @SerializedName("user") var user: UserModel = UserModel()
)
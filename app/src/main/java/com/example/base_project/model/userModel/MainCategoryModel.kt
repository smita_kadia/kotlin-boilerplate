package com.example.base_project.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MainCategoryModel  : CommonResponseModel() {

    @SerializedName("franquicias")
    @Expose
    var franquicias: MutableList<Franquicia>? = null
  //  var userList: MutableList<User>? = null


    class Franquicia {


        @SerializedName("APIKEY")
        @Expose
        var apikey: String? = null

        @SerializedName("tokenInvu")
        @Expose
        var tokenInvu: String? = null

        @SerializedName("negocio")
        @Expose
        var negocio: String? = null

        @SerializedName("principal")
        @Expose
        var principal: Boolean? = null

        @SerializedName("id_franquicia")
        @Expose
        var idFranquicia: String? = null

        @SerializedName("franquicia")
        @Expose
        var franquicia: String? = null

        @SerializedName("horaCierreLocal")
        @Expose
        var horaCierreLocal: String? = null


    }
}

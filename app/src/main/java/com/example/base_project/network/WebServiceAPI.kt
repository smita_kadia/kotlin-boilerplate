package com.example.base_project.network

import com.example.base_project.model.MainCategoryModel
import com.example.base_project.model.SubCategoryModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header


interface WebServiceAPI {



    @GET("index.php?r=configuraciones/franquicias")
    fun getCategoryList(@Header("APIKEY") lang: String?): Call<MainCategoryModel>

    @GET("index.php?r=menu")
    fun getSubCategoryList(@Header("APIKEY") lang: String?): Call<SubCategoryModel>


}